# Uses python3
import sys


def get_number_of_inversions(a):
    if len(a) <= 1:
        return a, 0

    mid = len(a) // 2
    l_array = a[:mid]
    r_array = a[mid:]

    l_array, inv_left = get_number_of_inversions(l_array)
    r_array, inv_right = get_number_of_inversions(r_array)
    sorted_a, inv_between = get_number_of_inversions_between(l_array, r_array)

    inversions = inv_left + inv_right + inv_between
    return sorted_a, inversions


def get_number_of_inversions_between(a, b):
    if len(b) == 0 or len(a) == 0:
        sorted_a = a + b
        return sorted_a, 0

    inversions, i, j = 0, 0, 0
    sorted_a = []
    while len(a) > 0 or len(b) > 0:
        if len(a) == 0:
            sorted_a += b
            del b[:]
        elif len(b) == 0:
            sorted_a += a
            del a[:]
        elif a[0] > b[0]:
            sorted_a.append(b[0])
            inversions += len(a)
            del b[0]
        else:
            sorted_a.append(a[0])
            del a[0]

    return sorted_a, inversions


def check():
    assert get_number_of_inversions([2,3])[1] == 0
    assert get_number_of_inversions([3,2])[1] == 1

    test = get_number_of_inversions([2,3,9,2,9])[1]
    assert test == 2
    print("Tests are ok.")


if __name__ == '__main__':
    # check()

    input = sys.stdin.read()
    n, *a = list(map(int, input.split()))
    print(get_number_of_inversions(a)[1])
