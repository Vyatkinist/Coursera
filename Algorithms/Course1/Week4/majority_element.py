# Uses python3
import sys


def get_majority_element(a):
    length = len(a)
    if length == 1:
        return a[0]
    half = length // 2

    el1 = get_majority_element(a[0:half])
    el2 = get_majority_element(a[half:length])

    if el1 == el2:
        return el1

    el1_freq = get_frequency(a, el1)
    el2_freq = get_frequency(a, el2)

    if el1_freq > half:
        return el1
    if el2_freq > half:
        return el2
    return -1


def get_frequency(a, el):
    counter = 0
    for item in a:
        if el == item:
            counter += 1

    return counter


def check():
    a = [2, 3, 9, 2, 2]
    assert get_majority_element(a) == 2

    a = [1,1,1,1,1,1,1,4,4,4,4,4,4]
    assert get_majority_element(a) == 1

    a = [2, 124554847, 2, 941795895, 2, 2, 2, 2, 792755190, 756617003]
    assert get_majority_element(a) == 2
    print("Tests are ok.")


if __name__ == '__main__':
    # check()

    input = sys.stdin.read()
    n, *a = list(map(int, input.split()))
    if get_majority_element(a) != -1:
        print(1)
    else:
        print(0)
