# Uses python3
import sys


def print_matrix(matrix):
    print('\n'.join([''.join(['{:4}'.format(item) for item in row])
                     for row in matrix]))


def evalt(a, b, op):
    if op == '+':
        return a + b
    elif op == '-':
        return a - b
    elif op == '*':
        return a * b
    else:
        assert False


def min_and_max(m, M, op, i, j):
    min_val = 1000 #sys.maxsize
    max_val = -1000 #-sys.maxsize

    if j <= i:
        return m[i][j], M[i][j]

    for k in range(i, j):
        a = evalt(M[i][k], M[k + 1][j], op[k])
        b = evalt(M[i][k], m[k + 1][j], op[k])
        c = evalt(m[i][k], M[k + 1][j], op[k])
        d = evalt(m[i][k], m[k + 1][j], op[k])

        # print('{0} {1} {2} {3} {4}'.format(min_val, a, b, c, d))
        min_val = min(min_val, a, b, c, d)

        # print(min_val)
        max_val = max(max_val, a, b, c, d)

    return min_val, max_val


def get_maximum_value(dataset):
    # declare variables
    d = []
    op = []
    M = []
    m = []

    # parse data
    raw_data = list(dataset)
    for i in range(len(raw_data)):
        if i % 2 == 0:
            d.append(raw_data[i])
        else:
            op.append(raw_data[i])

    # number of operands
    n = len(d)

    # fill initial data
    m = [[0 for x in range(n)] for x in range(n)]
    M = [[0 for x in range(n)] for x in range(n)]
    for i in range(0, n):
        m[i][i] = int(d[i])
        M[i][i] = int(d[i])

    # dynamic solution
    for s in range(0, n):
        for i in range(0, n-s):
            j = i + s
            # print('i: {0} j: {1}'.format(i, j))
            # print('{0} {1}'.format(i, j))
            min_v, max_v = min_and_max(m, M, op, i, j)
            # print('min: {0} max: {1}'.format(min_v, max_v))
            m[i][j], M[i][j] = min_v, max_v

            # print_matrix(m)
            # print('------')
            # print_matrix(M)
            # print('------')

    return M[0][n-1]


def check():
    assert get_maximum_value('1+5') == 6
    assert get_maximum_value(('5-8+7*4-8+9')) == 200
    print('Tests are ok.')


if __name__ == "__main__":
    # check()
    print(get_maximum_value(input()))