# Uses python3
import sys


def optimal_sequence(n):
    memo = []
    memo.append(0) # 0th element
    memo.append(0) # 1st element
    cur_n = 2
    while cur_n <= n:
        x, y, z = sys.maxsize, sys.maxsize, sys.maxsize

        if cur_n % 3 == 0:
            x = memo[cur_n // 3] + 1
        elif cur_n % 2 == 0:
            y = memo[cur_n // 2] + 1
        z = memo[cur_n - 1] + 1

        count = x if x < y else y
        count = count if count < z else z
        memo.append(count)

        cur_n += 1

    cur_n = n
    sequence = []
    while cur_n != 1:
        sequence.append(cur_n)
        x, y, z = sys.maxsize, sys.maxsize, sys.maxsize

        if cur_n % 3 == 0:
            x = memo[cur_n // 3]
        elif cur_n % 2 == 0:
            y = memo[cur_n // 2]
        z = memo[cur_n - 1]

        count = x if x < y else y
        count = count if count < z else z

        if count == x:
            cur_n //= 3
        elif count == y:
            cur_n //= 2
        else:
            cur_n -= 1

    sequence.append(1)
    return reversed(sequence)


def check():
    print(optimal_sequence(5))
    print(optimal_sequence(10))
    print(optimal_sequence(50))
    print(optimal_sequence(96234))

    test1 = optimal_sequence(5)
    list1 = []
    for i in test1:
        list1.append(i)
    #     print(i)
    assert len(list1) == 4
    assert list1 == [1, 2, 4, 5]

    test1 = optimal_sequence(96234)
    list1 = []
    for i in test1:
        list1.append(i)
        print(i)
    assert len(list1) == 15
    assert list1 == [1, 3, 9, 10, 11, 22, 66, 198, 594, 1782, 5346, 16038, 16039, 32078, 96234]

    print("Tests are ok.")


# check()
input = sys.stdin.read()
n = int(input)
sequence = list(optimal_sequence(n))
print(len(sequence) - 1)
for x in sequence:
    print(x, end=' ')
