# Uses python3
import sys

def gcd(a, b):
    if b == 0:
        return a

    remainder = a % b
    return gcd(b, remainder)

def lcm(a, b):
    gcd_calculated = gcd(a, b)

    return a // gcd_calculated * b

if __name__ == "__main__":
    input = sys.stdin.read()
    a, b = map(int, input.split())
    print(lcm(a, b))