# Uses python3
def calc(n):
    first = 0
    second = 1
    while (n > 1):
        n = n - 1
        temp = second
        second = (second + first) % 10
        first = temp

    return second

n = int(input())
print(calc(n))