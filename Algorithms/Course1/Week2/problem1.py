# Uses python3
def calc_fib(fib_numbers, n):
    if (n <= 1):
        fib_numbers[n] = n
        return n

    if (n > 1):
        if fib_numbers[n] != 0:
            return fib_numbers[n]
        else:
            fib_numbers[n] = calc_fib(fib_numbers, n - 1) + calc_fib(fib_numbers, n - 2)

    return fib_numbers[n]

n = int(input())
print(calc_fib([0 for x in range(n + 1)], n))