# Uses python3
import sys


remainders = []


def calc_fib(n):
    fib1 = 0
    fib2 = 1
    while n > 1:
        n -= 1
        fib2 += fib1
        fib1 = fib2 - fib1

    return fib2


def get_period_length(m):
    # condition for a period start is 0,1 number sequence
    counter = 1
    # keep two last Pisano period numbers
    period1 = 0
    period2 = 1

    while counter <= 2 or ((not (period1 == 0 and period2 == 1)) and counter > 2):
        next_period = (period1 % m + period2 % m) % m
        period1 = period2
        period2 = next_period

        remainders.append(period1)

        counter += 1

    # period1 is always calculated 1 fibonacci number ahead of the calculated period
    return counter - 1


def get_fibonacci_huge(n, m):
    period = get_period_length(m)
    remainder = n % period

    return remainders[remainder - 1]


if __name__ == '__main__':
    input = sys.stdin.read();
    n, m = map(int, input.split())
    print(get_fibonacci_huge(n, m))