# Uses python3
import sys


def get_optimal_value(capacity, weights, values):
    tuples = list()
    for item in range(len(weights)):
        tuples.append((weights[item], values[item]))

    tuples = sort_by_value(tuples)

    value = 0
    current_item = 0
    while capacity != 0 and current_item < len(tuples):
        if capacity >= tuples[current_item][0]:
            capacity -= tuples[current_item][0]
            value += tuples[current_item][1]
        else:
            value += capacity / tuples[current_item][0] * tuples[current_item][1]
            capacity = 0

        current_item += 1

    return value


def sort_by_value(tuples):
    sorted_tuples = []
    while len(tuples) > 0:
        max_value = 0
        max_index = -1
        for i in range(len(tuples)):
            current_value = tuples[i][1] / tuples[i][0]  # 0 is a weight, 1 is a value
            if current_value >= max_value:
                max_value = current_value
                max_index = i

        sorted_tuples.append(tuples[max_index])
        tuples.pop(max_index)

    return sorted_tuples


def not_a_test():
    # first comes value, then weight
    print ("working")
    #assert get_optimal_value(5, [10], [10]) == 5
    #assert get_optimal_value(10, [5], [5]) == 5
    assert get_optimal_value(50, [20, 50, 30], [60, 100, 120]) == 180
    assert get_optimal_value(10, [30], [500]) == (166 + 2/3)
    print ("done")


if __name__ == "__main__":
    # not_a_test()

    data = list(map(int, sys.stdin.read().split()))
    n, capacity = data[0:2]
    values = data[2:(2 * n + 2):2]
    weights = data[3:(2 * n + 2):2]
    opt_value = get_optimal_value(capacity, weights, values)
    print("{:.10f}".format(opt_value))
