# Uses python3
import sys
from collections import namedtuple

Segment = namedtuple('Segment', 'start end')


def sort_by_right_point(segments):
    sorted_segments = []
    while len(segments) > 0:
        min_value = segments[0][1]
        min_index = 0
        for i in range(len(segments)):
            if segments[i][1] <= min_value:
                min_value = segments[i][1]
                min_index = i

        sorted_segments.append(segments[min_index])
        segments.pop(min_index)

    return sorted_segments


def optimal_points(segments):
    segments = sort_by_right_point(segments)

    points = []
    while len(segments) > 0:
        point = segments[0][1]
        points.append(point)
        segments.pop(0)

        while len(segments) > 0 and point >= segments[0][0]:
            segments.pop(0)

    return points


def check():
    points = optimal_points([(1,3), (2,5), (3,6)])
    assert len(points) == 1
    points = optimal_points([(4,7), (1,3), (2,5), (5,6)])
    assert len(points) == 2


if __name__ == '__main__':
    # check()

    input = sys.stdin.read()
    n, *data = map(int, input.split())
    segments = list(map(lambda x: Segment(x[0], x[1]), zip(data[::2], data[1::2])))
    points = optimal_points(segments)
    print(len(points))
    for p in points:
        print(p, end=' ')
