# python3

from sys import stdin


# Splay tree implementation

# Vertex of a splay tree
class Vertex:
    def __init__(self, key, sum, left, right, parent):
        (self.key, self.sum, self.left, self.right, self.parent) = (key, sum, left, right, parent)


def update(v):
    if v == None:
        return
    v.sum = v.key + (v.left.sum if v.left != None else 0) + (v.right.sum if v.right != None else 0)
    if v.left != None:
        v.left.parent = v
    if v.right != None:
        v.right.parent = v


def smallRotation(v):
    parent = v.parent
    if parent == None:
        return
    grandparent = v.parent.parent
    if parent.left == v:
        m = v.right
        v.right = parent
        parent.left = m
    else:
        m = v.left
        v.left = parent
        parent.right = m
    update(parent)
    update(v)
    v.parent = grandparent
    if grandparent != None:
        if grandparent.left == parent:
            grandparent.left = v
        else:
            grandparent.right = v


def bigRotation(v):
    if v.parent.left == v and v.parent.parent.left == v.parent:
        # Zig-zig
        smallRotation(v.parent)
        smallRotation(v)
    elif v.parent.right == v and v.parent.parent.right == v.parent:
        # Zig-zig
        smallRotation(v.parent)
        smallRotation(v)
    else:
        # Zig-zag
        smallRotation(v);
        smallRotation(v);


# Makes splay of the given vertex and makes
# it the new root.
def splay(v):
    if v == None:
        return None
    while v.parent != None:
        if v.parent.parent == None:
            smallRotation(v)
            break
        bigRotation(v)
    return v


# Searches for the given key in the tree with the given root
# and calls splay for the deepest visited node after that.
# Returns pair of the result and the new root.
# If found, result is a pointer to the node with the given key.
# Otherwise, result is a pointer to the node with the smallest
# bigger key (next value in the order).
# If the key is bigger than all keys in the tree,
# then result is None.
def find(root, key):
    v = root
    last = root
    next = None
    while v != None:
        if v.key >= key and (next == None or v.key < next.key):
            next = v
        last = v
        if v.key == key:
            break
        if v.key < key:
            v = v.right
        else:
            v = v.left
    root = splay(last)
    return (next, root)


def split(root, key):
    (result, root) = find(root, key)
    if result == None:
        return (root, None)
    right = splay(result)
    left = right.left
    right.left = None
    if left != None:
        left.parent = None
    update(left)
    update(right)
    return (left, right)


def merge(left, right):
    if left == None:
        return right
    if right == None:
        return left
    while right.left != None:
        right = right.left
    right = splay(right)
    right.left = left
    update(right)
    return right


# Code that uses splay tree to solve the problem

root = None


def insert(x):
    global root
    (left, right) = split(root, x)
    new_vertex = None
    if right == None or right.key != x:
        new_vertex = Vertex(x, x, None, None, None)
    root = merge(merge(left, new_vertex), right)


def erase(x):
    # my code
    global root
    node_to_delete = binary_search(root, x)
    if node_to_delete is None:
        return

    if node_to_delete.key != x: # not found, do nothing
        return

    root = splay(node_to_delete)
    (left, right) = split(root, root.key)
    right = root.right
    if right is not None:
        right.parent = None
    root = merge(left, right)


def search(x):
    # my code
    global root
    result = find(root, x)
    search_node = result[0]
    root = result[1]
    if result is None or search_node is None or search_node.key != x:
        return False

    return True


def sum(fr, to):
    # my code
    global root
    (left, middle) = split(root, fr)
    (middle, right) = split(middle, to + 1)

    answer = middle.sum if middle is not None else 0

    merged = merge(left, middle)
    merged = merge(merged, right)
    root = merged
    return answer

#
# def sum(fr, to):
#     global root
#     (left, middle) = split(root, fr)
#     (middle, right) = split(middle, to + 1)
#     ans = 0
#     # Complete the implementation of sum
#
#     return ans


# my code
def binary_search(rootnode, x):
    if rootnode is None:
        return None

    if rootnode.key == x:
        return rootnode

    if x > rootnode.key:
        if rootnode.right is not None:
            return binary_search(rootnode.right, x)
        else:
            return rootnode
    elif x < rootnode.key:
        if rootnode.left is not None:
            return binary_search(rootnode.left, x)
        else:
            return rootnode

    return None


def traverse(root):
    if root is None:
        return

    thislevel = [root]
    space_quotient = 10
    while thislevel:
        nextlevel = list()
        for n in thislevel:
            # print parent
            parent = ""
            if hasattr(n, 'parent'):
                parent = str(n.parent.key) if n.parent is not None else ""

            # print sum
            sum = ""
            if hasattr(n, 'sum'):
                sum = str(n.sum)

            # print whole node
            print((" " * space_quotient) + str(n.key) + " sum(" + sum + ")" + " parent(" + parent + ")", end="", flush=True)

            if n.left: nextlevel.append(n.left)
            if n.right: nextlevel.append(n.right)
        if space_quotient >= 1:
            space_quotient -= 3
        print()
        thislevel = nextlevel
    print("--------------------------------")

######################################################################
import random

def random_test(times):
    global root
    root = None
    maxnumber = 10 #as you need
    operations = []
    values = []
    called = 0
    while called < times:
        called += 1
        op = random.randrange(5)
        if op == 0 or op == 1:
            arg = random.randrange(maxnumber) + 1
            insert(arg)
            operations.append("insert(" + str(arg) +")")
            if arg not in values:
              values.append(arg)
        if op == 2:
            if len(values) > 0:
                index = random.randrange(len(values))
                arg = values[index]
            else:
                arg = random.randrange(maxnumber) + 1
            erase(arg)
            operations.append("erase(" + str(arg)+")")
            if arg in values:
              values.remove(arg)

        if op == 3:
            prob = random.randrange(2)
            if prob == 0:
                if len(values) > 0:
                    index = random.randrange(len(values))
                    arg = values[index]
                else:
                    arg = random.randrange(maxnumber) + 1
            else:
                arg = random.randrange(maxnumber)
            ret = search(arg)
            operations.append("search(" + str(arg)+")")
            if ret and arg not in values:
                print("Found non-existing:")
                print("Found: " + str(ret) if ret is not None else ret)
                print("Searched: " + str(arg))
                print("failed search " + str(arg))
                print(operations)
                print(values)
                traverse(root)
                break
            if not ret and arg in values:
                print("Not found existing:")
                print("Found: " + str(ret) if ret is not None else ret)
                print("Searched: " + str(arg))
                print("failed search " + str(arg))
                print(operations)
                print(values)
                traverse(root)
                break

        if op == 4:
            arg = random.randrange(maxnumber//3)
            arg2 = random.randrange(arg, maxnumber)
            test = sum(arg, arg2)
            total = 0
            if arg != arg2:
                for i in range(len(values)):
                    x = values[i]
                    if x >= arg and x <= arg2:
                        total += x
            operations.append("test = dosum(" + str(arg) +", " + str(arg2) +")")
            if test != total:
                print("failed sum got: {0} expected:".format(test, total))
                print(operations)
                print(values)
                traverse(root)
                break
        print("all is going well")
######################################################################
# if __name__ == '__main__':
#     # for i in range(6666):
#     #     random_test(666)
#     insert(10)
#     traverse(root)
#     print(sum(1, 7))
#     traverse(root)
#     insert(1)
#     traverse(root)
#     print(sum(1, 1))
#     traverse(root)


MODULO = 1000000001
n = int(stdin.readline())
last_sum_result = 0
for i in range(n):
    line = stdin.readline().split()
    if line[0] == '+':
        x = int(line[1])
        insert((x + last_sum_result) % MODULO)
    elif line[0] == '-':
        x = int(line[1])
        erase((x + last_sum_result) % MODULO)
    elif line[0] == '?':
        x = int(line[1])
        print('Found' if search((x + last_sum_result) % MODULO) else 'Not found')
    elif line[0] == 's':
        l = int(line[1])
        r = int(line[2])
        res = sum((l + last_sum_result) % MODULO, (r + last_sum_result) % MODULO)
        print(res)
        last_sum_result = res % MODULO