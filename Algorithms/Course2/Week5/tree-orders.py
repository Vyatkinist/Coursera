# python3

import sys, threading, queue

sys.setrecursionlimit(10 ** 6)  # max depth of recursion
threading.stack_size(2 ** 27)  # new thread will get stack of such size


class TreeOrders:
    def read(self):
        self.n = int(readline())
        self.key = [0 for i in range(self.n)]
        self.left = [0 for i in range(self.n)]
        self.right = [0 for i in range(self.n)]
        for i in range(self.n):
            [a, b, c] = map(int, readline().split())
            self.key[i] = a
            self.left[i] = b
            self.right[i] = c
        self.breadth_queue = queue.Queue()

    def inOrder(self):
        self.result = []
        # Finish the implementation
        # You may need to add a new recursive method to do that
        self.print_node_inorder(0)
        return self.result

    def print_node_inorder(self, index):
        if index == -1:
            return

        # print left
        left_index = self.left[index]
        self.print_node_inorder(left_index)

        # print root
        self.result.append(self.key[index])

        # print right
        right_index = self.right[index]
        self.print_node_inorder(right_index)

    def preOrder(self):
        self.result = []
        # Finish the implementation
        # You may need to add a new recursive method to do that
        self.print_node_preorder(0)
        return self.result

    def print_node_preorder(self, index):
        if index == -1:
            return

        # print root
        self.result.append(self.key[index])

        # print left
        left_index = self.left[index]
        self.print_node_preorder(left_index)

        # print right
        right_index = self.right[index]
        self.print_node_preorder(right_index)

    def postOrder(self):
        self.result = []
        # Finish the implementation
        # You may need to add a new recursive method to do that
        self.print_node_postorder(0)
        return self.result

    def print_node_postorder(self, index):
        if index == -1:
            return

        # print left
        left_index = self.left[index]
        self.print_node_postorder(left_index)

        # print right
        right_index = self.right[index]
        self.print_node_postorder(right_index)

        # print root
        self.result.append(self.key[index])

    def breadth_first(self):
        self.result = []
        # Finish the implementation
        # You may need to add a new recursive method to do that
        self.breadth_queue.put(self.key[0])
        self.print_node_breadth(0)
        return self.breadth_queue

    def print_node_breadth(self, index):
        if index == -1:
            return

        self.breadth_queue.put(self.key[index])

        # print left
        left_index = self.left[index]
        self.print_node_breadth(left_index)

        # print right
        right_index = self.right[index]
        self.print_node_breadth(right_index)


times_called = 0
def readline():
    global times_called
    inputs_list = [
        "5",
        "4 1 2",
        "2 3 4",
        "5 -1 -1",
        "1 -1 -1",
        "3 -1 -1"
    ]

    result = inputs_list[times_called]
    times_called += 1
    return result

    #return sys.stdin.readline()


def main():
    q = queue.Queue()
    q.put(132)
    q.put(666)
    print(q.get())
    print(q.get())
    # tree = TreeOrders()
    # tree.read()
    # print(" ".join(str(x) for x in tree.inOrder()))
    # print(" ".join(str(x) for x in tree.preOrder()))
    # print(" ".join(str(x) for x in tree.postOrder()))


threading.Thread(target=main).start()
