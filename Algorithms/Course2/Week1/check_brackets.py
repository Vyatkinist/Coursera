# python3

import sys

def match(open, close):
    if open == '[' and close == ']':
        return True
    if open == '{' and close == '}':
        return True
    if open == '(' and close == ')':
        return True
    return False

def check(text):
    stack = []
    for i, next in enumerate(text):
        if next == '(' or next == '[' or next == '{':
            stack.append(next)
        elif next == ')' or next == ']' or next == '}':
            if len(stack) == 0:
                return 1

            stack.append(next)
            opening = stack[len(stack) - 2]
            closing = stack[len(stack) - 1]
            is_match = match(opening, closing)
            if not is_match:
                return len(stack)
            else:
                stack.pop()
                stack.pop()

    if len(stack) != 0:
        return len(stack)
    return "Success"

if __name__ == "__main__":
    #text = sys.stdin.read()
    text = "(){[}"
    print (check(text))
