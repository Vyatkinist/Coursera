# python3
import sys
import os
import time


class Node:
    def __init__(self, value, next):
        self.value = value
        self.next = next


class Queue:
    def __init__(self, size):
        self.head = None
        self.tail = None
        self.size = size
        self.length = 0 # might be unnecessary

    def is_empty(self):
        return self.head is None

    def is_full(self):
        return self.size == self.length

    def push(self, value):
        if self.is_full():
            return

        self.length += 1

        value_node = Node(value, None)
        if self.head is None:
            self.head = value_node
            self.tail = value_node
            return

        self.tail.next = value_node
        self.tail = value_node

    def pop(self):
        if self.is_empty():
            return None

        self.length -= 1

        return_node = self.head
        self.head = self.head.next
        return return_node.value

    def peek_first(self):
        if self.is_empty():
            return None
        return self.head.value

    def peek_last(self):
        if self.is_empty():
            return None
        return self.tail.value

    def print(self):
        next = self.head
        while next is not None:
            print(next.value)
            next = next.next


class Request:
    def __init__(self, arrival_time, process_time):
        self.arrival_time = arrival_time
        self.process_time = process_time


class Response:
    def __init__(self, dropped, start_time):
        self.dropped = dropped
        self.start_time = start_time


class Buffer:
    def __init__(self, size):
        self.size = size
        self.finish_times = Queue(size)
        self.start_times = Queue(size)
        self.current_time = 0

    def process(self, request):
        # write your code here

        # free the queue from already processed events by this time
        requests_popped = 0

        while self.finish_times.peek_first() is not None and self.finish_times.peek_first() <= request.arrival_time:
            self.finish_times.pop()
            requests_popped += 1
            if self.finish_times.is_empty():
                break

        if self.finish_times.is_full():# and requests_popped == 0:
            return Response(dropped=True, start_time=-1)

        start_time = 0
        # if the queue is not full, enqueue the incoming request
        if not self.finish_times.is_full():
            # if the last item in the queue exists, get his finish time
            last_request = self.finish_times.peek_last()
            last_item_finish_time = last_request if last_request is not None else 0

            # push the time when this request will be processed
            start_time = request.arrival_time if last_item_finish_time < request.arrival_time else last_item_finish_time
            self.finish_times.push(start_time + request.process_time)

        return Response(dropped=False, start_time=start_time)


def read_requests(count):
    requests = []
    for i in range(count):
        arrival_time, process_time = map(int, sys.stdin.readline().strip().split())
        requests.append(Request(arrival_time, process_time))
    return requests


def process_requests(requests, buffer):
    responses = []
    for request in requests:
        responses.append(buffer.process(request))

    return responses


def print_responses(responses):
    for response in responses:
        print(response.start_time if not response.dropped else -1)


def read_inputs_from_file(name):
    rootpath = "/Users/Vyatkinist/Documents/Coursera/Algorithms/Course2/Week1/tests/"
    testfiles = os.listdir(rootpath)
    with open(rootpath + name, "r") as ins:
        array = []
        for line in ins:
            array.append(line[0:len(line) - 1])
    return array


def check(inputs, correct_outputs):
    size, count = map(int, inputs[0].strip().split())

    request_inputs = inputs[1:]
    requests = []
    for i in range(count):
        arrival_time, process_time = map(int, request_inputs[i].strip().split())
        requests.append(Request(arrival_time, process_time))

    buffer = Buffer(size)
    responses = process_requests(requests, buffer)

    responses_formatted = []
    for i in range(count):
        responses_formatted.append(responses[i].start_time)

    for i in range(count):
        failed = responses_formatted[i] != correct_outputs[i]
        if failed:
            print("Input")
            for j in range(len(inputs)):
                print(inputs[j])
            print("Got:")
            for l in range(len(responses_formatted)):
                print(responses_formatted[l])
            print("Expected:")
            for k in range(len(correct_outputs)):
                print(correct_outputs[k])

            time.sleep(0.1)
            raise "Test failed"


def check_all():
    check(["1 0"], "")
    check(
        inputs=[
            "1 1",
            "0 0"],
        correct_outputs=[0])
    check(
        inputs=[
            "1 2",
            "0 1",
            "0 1"],
        correct_outputs=[0, -1])
    check(
        inputs=[
            "1 2",
            "0 1",
            "1 1"],
        correct_outputs=[0, 1])
    check(
        inputs=[
            "1 1",
            "1 0"],
        correct_outputs=[1])
    check(
        inputs=[
            "2 2",
            "0 1",
            "0 1"],
        correct_outputs=[0, 1])
    check(
        inputs=[
            "1 2",
            "0 0",
            "0 0"],
        correct_outputs=[0, 0])
    check(
        inputs=[
            "2 3",
            "0 2",
            "1 4",
            "5 3"],
        correct_outputs=[0, 2, 6])

    print("All tests are fine.")


if __name__ == "__main__":
    # tests
    #check_all()

    size, count = map(int, sys.stdin.readline().strip().split())
    requests = read_requests(count)

    buffer = Buffer(size)
    responses = process_requests(requests, buffer)

    print_responses(responses)


