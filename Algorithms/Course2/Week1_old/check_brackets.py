# python3

import sys


def is_opening_bracket(char):
    return char in '[({'


def is_closing_bracket(char):
    return char in '])}'


def is_match(open, close):
    if open == '[' and close == ']':
        return True
    if open == '{' and close == '}':
        return True
    if open == '(' and close == ')':
        return True
    return False


def check_string(str):
    stack = []
    id_stack = []

    char_index = 0
    for i in range(0, len(str)):
        char = str[i]
        if is_opening_bracket(char):
            stack.append(char)
            id_stack.append(i)
        elif is_closing_bracket(char):
            if len(stack) == 0:
                print(i + 1)
                return

            top_char = stack.pop()
            id_stack.pop()
            if not is_match(top_char, char):
                print(i + 1)
                return

    if len(stack) == 0:
        print("Success")
    else:
        print(id_stack.pop() + 1)


def check():
    check_string('[]')
    check_string('{}[]')
    check_string('[()]')
    check_string('(())')
    check_string('{[]}()')
    check_string('{')
    check_string('{[}')
    check_string('foo(bar);')
    check_string('foo(bar[i);')
    check_string('yolo(swaggerino()666')
    check_string('}')

if __name__ == "__main__":
    # check()
    text = sys.stdin.read()
    check_string(text)