# python3

import sys, threading
sys.setrecursionlimit(10**7) # max depth of recursion
threading.stack_size(2**25)  # new thread will get stack of such size


class TreeHeight:
    def __init__(self, n, parents):
        self.n = len(parents)
        self.parents = parents
        self.memo = [-1 for i in range(n)]

    def compute_height(self):
        max_height = 0
        for i in range(self.n):
            height = 0
            parent = self.parents[i]
            #print(parent)

            while parent != -1:
                height += 1
                parent = self.parents[parent]

                self.memo[i] = height

            max_height = max_height if max_height > height else height
        return max_height

def compute(n_str, parents_str):
    n = int(n_str)
    parents = list(map(int, parents_str.split()))
    tree = TreeHeight(n, parents)
    print(tree.compute_height() + 1)


def check():
    compute('5', '4 -1 4 1 1')
    compute('5', '-1 0 4 0 3')


if __name__ == "__main__":
    check()

#
# def main():
#     compute(sys.stdin.readline(), sys.stdin.readline())
#
# threading.Thread(target=main).start()
