# python3


class HeapBuilder:
    def __init__(self):
        self._swaps = []
        self._data = []
        self._size = 0

    def read_data(self, count, data):
        n = int(count)
        self._data = [int(s) for s in data.split()]
        self._size = len(self._data)
        assert n == self._size

    def write_response(self):
        print(len(self._swaps))
        for swap in self._swaps:
            print(swap[0], swap[1])

    def generate_swaps(self):
        to = int(self._size / 2)
        for i in reversed(range(to)):
            self.sift_down(i)

    def left_child(self, i):
        return 2 * i + 1

    def right_child(self, i):
        return 2 * i + 2

    def parent(self, i):
        return (i - 1) / 2

    def sift_down(self, i):
        size = len(self._data)
        arr = self._data
        min_id = i
        l = self.left_child(i)
        if l < size and arr[l] < arr[min_id]:
            min_id = l
        r = self.right_child(i)
        if r < size and arr[r] < arr[min_id]:
            min_id = r
        if i != min_id:
            temp = arr[i]
            arr[i] = arr[min_id]
            arr[min_id] = temp
            self._swaps.append((i, min_id))
            self.sift_down(min_id)

    def solve(self, n, data):
        self.read_data(n, data)
        self.generate_swaps()
        self.write_response()


if __name__ == '__main__':
    heap_builder = HeapBuilder()
    n = input()
    data = input()
    # n = 5
    # data = "5 4 3 2 1"
    heap_builder.solve(n, data)
