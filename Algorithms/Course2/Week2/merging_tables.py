# python3
import sys

num_tables = None
num_merges = None
table_rows = None
links = None

rank = None
parent = None
max_rows = 0

def init(first_line, second_line):
    global num_tables
    global num_merges
    global table_rows
    global links
    global rank
    global parent
    global max_rows

    #first_input_line = sys.stdin.readline()
    #second_input_line = sys.stdin.readline()
    num_tables, num_merges = map(int, first_line.split())
    table_rows = list(map(int, second_line.split()))

    rank = [1] * num_tables
    parent = list(range(0, num_tables))
    max_rows = max(table_rows)


def get_parent(table):
    global num_tables
    global num_merges
    global table_rows
    global links
    global rank
    global parent
    global max_rows

    if table != parent[table]:
        return get_parent(parent[table])

    return table


def find(i):
    global num_tables
    global num_merges
    global table_rows
    global links
    global rank
    global parent
    global max_rows

    while parent[i] != i:
        i = parent[i]
    return i


def merge(destination, source):
    global num_tables
    global num_merges
    global table_rows
    global links
    global rank
    global parent
    global max_rows

    realDestination, realSource = get_parent(destination), get_parent(source)

    if realDestination == realSource:
        return False

    # merge two components
    # use union by rank heuristic 
    # update ans with the new maximum table size

    if rank[realDestination] >= rank[realSource]:
        # attach real source to real destination
        parent[realSource] = realDestination

        # move rows from real source to real destination
        table_rows[realDestination] += table_rows[realSource]
        table_rows[realSource] = 0

        # update max value
        if table_rows[realDestination] > max_rows:
            max_rows = table_rows[realDestination]

        # increase rank if ranks are equal
        if rank[realDestination] == rank[realSource]:
            rank[realDestination] += 1
    else:
        # attach real destination to real source
        parent[realDestination] = realSource

        table_rows[realSource] += table_rows[realDestination]
        table_rows[realDestination] = 0

        if table_rows[realSource] > max_rows:
            max_rows = table_rows[realSource]

    return True


def merge_by_input(input_func):
    global num_tables
    global num_merges
    global table_rows
    global links
    global rank
    global parent
    global max_rows

    for i in range(num_merges):
        #next_input = ["3 5", "2 4", "1 4", "5 4", "5 3"]
        #next_input = ["6 6", "6 5", "5 4", "4 3"]
        #destination, source = map(int, next_input[i].split())
        destination, source = map(int, sys.stdin.readline().split())
        merge(destination - 1, source - 1)
        print(max_rows)


def from_stdin():
    global num_tables
    global num_merges
    global table_rows
    global links
    global rank
    global parent
    global max_rows

    return sys.stdin.readline()


def from_test1():
    input_list = ["3 5", "2 4", "1 4", "5 4", "5 3"]
    for i in range(len(input_list)):
        print(input_list[i])
        yield input_list[i]


def check():
    global num_tables
    global num_merges
    global table_rows
    global links
    global rank
    global parent
    global max_rows

    #init("5 5", "1 1 1 1 1")
    init("6 4", "10 0 5 0 3 3")
    merge_by_input(from_test1)
    
if __name__ == '__main__':

    init(sys.stdin.readline(), sys.stdin.readline())
    merge_by_input(from_stdin)

    #check()
