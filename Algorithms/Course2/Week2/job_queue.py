# python3
import heapq


class JobQueue:
    def read_data(self, num_workers, num_jobs, jobs):
        self.num_workers = num_workers
        self.jobs = jobs
        self.worker_heap = []
        assert num_jobs == len(self.jobs)

    def write_response(self):
        for i in range(len(self.jobs)):
          print(self.assigned_workers[i], self.start_times[i]) 

    def assign_jobs(self):
        self.assigned_workers = [None] * len(self.jobs)
        self.start_times = [None] * len(self.jobs)

        # fill in the heap of workers first
        for i in range(self.num_workers):
            heapq.heappush(self.worker_heap, (0, i))

        # worker is a tuple in a format ({time_passed}, {worker_id})
        # time and id are flipped to prioritize time over id since this is how heapq prioritizes
        for i in range(len(self.jobs)):
            # get worker from a heap
            worker = heapq.heappop(self.worker_heap)

            # write data
            self.assigned_workers[i] = worker[1]
            self.start_times[i] = worker[0]
            worker = (worker[0] + self.jobs[i], worker[1])

            # push worker back to heap
            heapq.heappush(self.worker_heap, worker)

    def solve(self, num_workers, num_jobs, jobs):
        self.read_data(num_workers, num_jobs, jobs)
        self.assign_jobs()
        self.write_response()


def check():
    job_queue = JobQueue()
    job_queue.solve(2, 5, [1, 2, 3, 4, 5])
    job_queue.solve(4, 20, [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])


if __name__ == '__main__':
    #check()
    num_workers, num_jobs = map(int, input().split())
    jobs = list(map(int, input().split()))

    job_queue = JobQueue()
    job_queue.solve(num_workers, num_jobs, jobs)

