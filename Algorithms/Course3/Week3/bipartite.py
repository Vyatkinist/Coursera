#Uses python3

import sys
import queue


def bipartite(adj):
    colors = [None for i in range(len(adj))]
    s = 0
    colors[s] = True

    q = queue.Queue()
    q.put(s)

    while not q.empty():
        u = q.get()
        for v in adj[u]:
            if colors[v] == colors[u]:
                return 0
            if colors[v] == None:
                colors[v] = not colors[u]
                q.put(v)

    return 1

if __name__ == '__main__':
    input = sys.stdin.read()
    #input = "4 4\n1 2\n4 1\n2 3\n3 1"
    #input = "5 4\n5 2\n4 2\n3 4\n1 4"

    data = list(map(int, input.split()))
    n, m = data[0:2]
    data = data[2:]
    edges = list(zip(data[0:(2 * m):2], data[1:(2 * m):2]))
    adj = [[] for _ in range(n)]
    for (a, b) in edges:
        adj[a - 1].append(b - 1)
        adj[b - 1].append(a - 1)
    print(bipartite(adj))
