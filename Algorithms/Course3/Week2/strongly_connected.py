#Uses python3

import sys
import heapq

sys.setrecursionlimit(200000)


def number_of_strongly_connected_components(adj):
    visited = [False for i in range(len(adj))]

    rev_adj = revert_adj(adj)
    # make a heap
    pre_order = [-1 for i in range(len(adj))]
    post_order = []

    global order_clock
    order_clock = 0

    # DFS on reversed graph
    for v in range(len(rev_adj)):
        if not visited[v]:
            explore(v, rev_adj, visited, post_order)

    # check for SCCs
    scc_index = 0
    visited = [False for i in range(len(adj))]
    while len(post_order) > 0:
        # retrieve by max post order on reversed graph
        # max post order on reversed graph means sink SCC element on graph
        v = heapq.heappop(post_order)[1]
        if not visited[v]:
            scc_index += 1
            explore_light(v, adj, visited)

    return scc_index


def explore_light(v, adj, visited):
    visited[v] = True

    for u in adj[v]:
        if not visited[u]:
            explore_light(u, adj, visited)


def explore(v, adj, visited, post_order):
    global order_clock
    visited[v] = True

    # pre-order action
    order_clock += 1

    for u in adj[v]:
        if not visited[u]:
            explore(u, adj, visited, post_order)

    # post-order action
    # record negative postorder to make largest postorder the minimum of the heap
    heapq.heappush(post_order, (-order_clock, v))
    order_clock += 1


def revert_adj(adj):
    rev_adj = [[] for i in range(len(adj))]

    for v in range(len(adj)):
        for u in adj[v]:
            rev_adj[u].append(v)

    return rev_adj


if __name__ == '__main__':
    input = sys.stdin.read()
    #input = "4 4\n1 2\n4 1\n2 3\n3 1"
    #input = "5 7\n2 1\n3 2\n3 1\n4 3\n4 1\n5 2\n5 3"

    data = list(map(int, input.split()))
    n, m = data[0:2]
    data = data[2:]
    edges = list(zip(data[0:(2 * m):2], data[1:(2 * m):2]))
    adj = [[] for _ in range(n)]
    for (a, b) in edges:
        adj[a - 1].append(b - 1)
    print(number_of_strongly_connected_components(adj))
