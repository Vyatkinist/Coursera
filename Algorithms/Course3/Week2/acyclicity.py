#Uses python3

import sys


global found_cycle


def acyclic(adj):
    global found_cycle
    found_cycle = False

    for v in range(len(adj)):
        visited = [False] * len(adj)

        if not visited[v]:
            explore(v, adj, visited, v)

            if found_cycle:
                return 1

    return 1 if found_cycle else 0


def explore(v, adj, visited, entry_node):
    global found_cycle

    if (found_cycle):
        return

    visited[v] = True

    for w in adj[v]:
        if w == entry_node:
            found_cycle = True

        if (not visited[w]):
            explore(w, adj, visited, entry_node)


if __name__ == '__main__':
    input = sys.stdin.read()
    #input = "4 4\n1 2\n4 1\n2 3\n3 1"
    #input = "5 7\n1 2\n2 3\n1 3\n3 4\n1 4\n2 5\n3 5"
    #input = "4 3\n1 2\n3 2\n4 3"

    data = list(map(int, input.split()))
    n, m = data[0:2]
    data = data[2:]
    edges = list(zip(data[0:(2 * m):2], data[1:(2 * m):2]))
    adj = [[] for _ in range(n)]
    for (a, b) in edges:
        adj[a - 1].append(b - 1)
    print(acyclic(adj))
