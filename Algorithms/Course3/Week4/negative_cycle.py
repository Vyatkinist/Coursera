#Uses python3

import sys


def negative_cycle(adj, cost):
    dist = [sys.maxsize for i in range(len(adj))]
    dist[0] = 0
    V = len(adj)

    for i in range(V):
        at_least_one_edge_relaxed = False
        for u in range(len(adj)):
            cost_counter = 0
            for v in adj[u]:
                if dist[v] > dist[u] + cost[u][cost_counter]:
                    dist[v] = dist[u] + cost[u][cost_counter]
                    at_least_one_edge_relaxed = True
                cost_counter += 1

        # we can safely assume there are no negative cycles when
        # there are no edges to relax on any iteration with number less than V-1
        if i < V - 1 and not at_least_one_edge_relaxed:
            break

        if i == V - 1 and at_least_one_edge_relaxed:
            return 1

    return 0


if __name__ == '__main__':
    input = sys.stdin.read()
    #input = "4 4\n1 2 -5\n4 1 2\n2 3 2\n3 1 1\n"

    data = list(map(int, input.split()))
    n, m = data[0:2]
    data = data[2:]
    edges = list(zip(zip(data[0:(3 * m):3], data[1:(3 * m):3]), data[2:(3 * m):3]))
    data = data[3 * m:]
    adj = [[] for _ in range(n)]
    cost = [[] for _ in range(n)]
    for ((a, b), w) in edges:
        adj[a - 1].append(b - 1)
        cost[a - 1].append(w)
    print(negative_cycle(adj, cost))
