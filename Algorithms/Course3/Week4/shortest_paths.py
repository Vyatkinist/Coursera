#Uses python3

import sys
import queue


def shortest_paths(adj, cost, s):
    dist = [float('inf') for i in range(len(adj))]
    dist[s] = 0
    V = len(adj)

    relaxed_vertices = []
    prev = [-1] * len(adj)
    for i in range(V - 1):
        at_least_one_edge_relaxed = False
        for u in range(len(adj)):
            cost_counter = 0
            for v in adj[u]:
                if dist[v] > dist[u] + cost[u][cost_counter]:
                    dist[v] = dist[u] + cost[u][cost_counter]
                    at_least_one_edge_relaxed = True
                    prev[v] = u
                cost_counter += 1

        # we can safely assume there are no negative cycles when
        # there are no edges to relax on any iteration with number less than V-1
        if i < V - 1 and not at_least_one_edge_relaxed:
            return dist

    # V-th iteration to detect vertices that have changed their distances
    at_least_one_edge_relaxed = False

    for u in range(len(adj)):
        cost_counter = 0
        for v in adj[u]:
            if dist[v] > dist[u] + cost[u][cost_counter]:
                dist[v] = dist[u] + cost[u][cost_counter]
                at_least_one_edge_relaxed = True
                prev[v] = u
                relaxed_vertices.append(v)
            cost_counter += 1

    # go back V times from one of relaxed vertices
    negatice_cycle_vertex = relaxed_vertices[0]
    for i in range(V):
        negatice_cycle_vertex = prev[negatice_cycle_vertex]

    # do BFS on this vertex
    negative_vertices = []
    visited = [False]*V
    q = queue.Queue()
    q.put(negatice_cycle_vertex)

    while q.qsize() > 0:
        u = q.get()
        visited[u] = True
        dist[u] = -1
        for v in adj[u]:
            if not visited[v]:
                q.put(v)

    return dist



if __name__ == '__main__':
    #input = sys.stdin.read()
    #input = "6 7\n1 2 10\n2 3 5\n1 3 100\n3 5 7\n5 4 10\n4 3 -18\n6 1 -1\n1"
    #input = "5 4\n1 2 1\n4 1 2\n2 3 2\n3 1 -5\n 4"
    #input = "9 10\n1 2 1\n2 3 1\n3 4 1\n4 2 -10\n4 5 20\n1 6 1\n6 7 1\n7 8 1\n8 6 -10\n8 9 20\n1" # two negative cycles
    input = "10 11\n6 1 -1\n1 2 10\n1 3 100\n2 3 5\n3 5 7\n3 7 2\n5 4 10\n5 8 3\n8 10 2\n4 3 -18\n4 9 5\n1"

    data = list(map(int, input.split()))
    n, m = data[0:2]
    data = data[2:]
    edges = list(zip(zip(data[0:(3 * m):3], data[1:(3 * m):3]), data[2:(3 * m):3]))
    data = data[3 * m:]
    adj = [[] for _ in range(n)]
    cost = [[] for _ in range(n)]
    for ((a, b), w) in edges:
        adj[a - 1].append(b - 1)
        cost[a - 1].append(w)
    s = data[0]
    s -= 1

    dist = shortest_paths(adj, cost, s)
    for x in range(n):
        if dist[x] == float('inf'):
            print('*')
        elif dist[x] < 0:
            print('-')
        else:
            print(dist[x])

