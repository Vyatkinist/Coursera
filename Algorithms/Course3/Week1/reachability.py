#Uses python3

import sys

visited = [];
cluster_indices = [];

def reach(adj, vertex1, vertex2):
    cluster_index = 0;
    for v in range(len(adj)):
        if not visited[v]:
            explore(v, adj, cluster_index)
            cluster_index += 1;

    return 1 if cluster_indices[vertex1] == cluster_indices[vertex2] else 0

def explore(v, adj, cluster_index):
    visited[v] = True;
    cluster_indices[v] = cluster_index;
    for w in adj[v]:
        if not visited[w]:
            explore(w, adj, cluster_index)

if __name__ == '__main__':
    input = sys.stdin.read()
    # input = "4 4\n1 2\n3 2\n4 3\n1 4\n1 4\n"
    # input = "4 2\n1 2\n3 2\n1 4"

    data = list(map(int, input.split()))
    n, m = data[0:2]
    data = data[2:]
    edges = list(zip(data[0:(2 * m):2], data[1:(2 * m):2]))
    adj = [[] for _ in range(n)]
    for (a, b) in edges:
        adj[a - 1].append(b - 1)
        adj[b - 1].append(a - 1)

    # pre-initialize the array of visited flags
    visited = [False]*len(adj);
    cluster_indices = [-1]*len(adj)

    vertex1 = data[len(data) - 2] - 1;
    vertex2 = data[len(data) - 1] - 1;
    print(reach(adj, vertex1, vertex2))
