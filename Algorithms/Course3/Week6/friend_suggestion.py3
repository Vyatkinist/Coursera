#!/usr/bin/python3

import sys
import queue

# think of more elegant solution
global can_search
can_search = True

class BiDij:
    def __init__(self, n):
        self.n = n;                             # Number of nodes
        self.inf = n*10**6                      # All distances in the graph are smaller
        self.dist = [self.inf] * n              # Initialize distances for forward and backward searches
        self.dist_rev = [self.inf] * n
        self.visited = [False]*n                # visited[v] == True iff v was visited by forward or backward search
        self.visited_rev = [False] * n          # keep reversed visited for now. todo check if necessary after algorithm is done
        self.workset = []                       # All the nodes visited by forward or backward search

    def clear(self):
        global can_search
        can_search = True
        # Reinitialize the data structures for the next query after the previous query.
        for v in self.workset:
            self.dist[v] = self.inf
            self.dist_rev[v] = self.inf
            self.visited[v] = False
            self.visited_rev[v] = False
        del self.workset[0:len(self.workset)]

    def visit(self, q, dist, adj, cost, visited):
        global can_search

        if not can_search:
            return

        u = queue.heappop(q)[1]

        visited[u] = True
        # todo append only unique if possible
        self.workset.append(u)
        cost_counter = 0
        for v in adj[u]:
            if not visited[v] and dist[v] > dist[u] + cost[u][cost_counter]:
                self.workset.append(v)
                dist[v] = dist[u] + cost[u][cost_counter]
                queue.heappush(q, (dist[v], v))
            cost_counter += 1

        # stop dijkstra if we encountered visited vertex
        if self.visited[u] and self.visited_rev[u]:
            can_search = False

    def query(self, adj, cost, s, t):
        global can_search
        self.clear()

        # reassign direct and reversed variables
        adj_rev = adj[1]
        adj = adj[0]
        cost_rev = cost[1]
        cost = cost[0]

        # two queues for direct and reversed dijkstras
        q = []
        q_rev = []

        # init priority queues with 's' and 't'
        self.dist[s] = 0
        self.dist_rev[t] = 0
        queue.heappush(q, (self.dist[s], s))
        queue.heappush(q_rev, (self.dist_rev[t], t))

        # todo don't make second visit() call if can_search was set to False on first visit() call
        while can_search and q and q_rev:
            self.visit(q, self.dist, adj, cost, self.visited)
            self.visit(q_rev, self.dist_rev, adj_rev, cost_rev, self.visited_rev)

        # find the minimum dist[u] + dist_rev[u]
        min_dist = self.inf
        for i in self.workset:
            if (self.visited[i] or self.visited_rev[i]) and min_dist > self.dist[i] + self.dist_rev[i]:
                min_dist = self.dist[i] + self.dist_rev[i]

        return -1 if min_dist == self.inf else min_dist


global readl_index
readl_index = 0
def readl():
    global readl_index
    #input = ["2 1", "1 2 1", "4", "1 1", "2 2", "1 2", "2 1"]
    #input = ["2 1", "1 2 1", "1", "2 2"]
    #input = ["4 4", "1 2 1", "4 1 2", "2 3 2", "1 3 5", "1", "1 3"]
    # input = ["5 20",
    #          "1 2 667",
    #          "1 3 677",
    #          "1 4 700",
    #          "1 5 622",
    #          "2 1 118",
    #          "2 3 325",
    #          "2 4 784",
    #          "2 5 11",
    #          "3 1 585",
    #          "3 2 956",
    #          "3 4 551",
    #          "3 5 559",
    #          "4 1 503",
    #          "4 2 722",
    #          "4 3 331",
    #          "4 5 366",
    #          "5 1 880",
    #          "5 2 883",
    #          "5 3 461",
    #          "5 4 228",
    #          "10",
    #          "1 1", # original value is 1 1
    #          "1 2",
    #          "1 3",
    #          "1 4",
    #          "1 5",
    #          "2 1",
    #          "2 2",
    #          "2 3",
    #          "2 4",
    #          "2 5"
    #          ]
    #readl_index += 1
    #return map(lambda x: int(x), input[readl_index - 1].split())

    return map(int, sys.stdin.readline().split())


if __name__ == '__main__':
    n,m = readl()
    adj = [[[] for _ in range(n)], [[] for _ in range(n)]]
    cost = [[[] for _ in range(n)], [[] for _ in range(n)]]
    for e in range(m):
        u,v,c = readl()
        adj[0][u-1].append(v-1)
        cost[0][u-1].append(c)
        adj[1][v-1].append(u-1)
        cost[1][v-1].append(c)
    t, = readl()
    bidij = BiDij(n)
    for i in range(t):
        s, t = readl()
        print(bidij.query(adj, cost, s-1, t-1))
