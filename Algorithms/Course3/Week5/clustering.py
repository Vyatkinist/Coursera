#Uses python3
import sys
import math
import heapq

class Expando(object):
    pass


def clustering(x, y, k):
    # all vertices as disjoint sets
    V = [Expando() for i in range(len(x))]
    for vertex in V:
        make_set(vertex)

    # use heap to sort the weights and add unique edges
    h = []
    # todo the cycles can be cut in half to avoid going through i >= j cases
    for i in range(len(x)):
        for j in range(len(y)):
            # go through only part of the ixj matrix upper than its diagonal, where i < j
            if i < j:
                w = distance(x, y, i, j)
                # the data stored has the format (weight, (node1, node2))
                heapq.heappush(h, (w, (i, j)))

    X = []
    sum = 0
    clusters = len(x)
    while clusters > k:
        edge = heapq.heappop(h)
        node_tuple = edge[1]
        u = node_tuple[0]
        v = node_tuple[1]

        findu = find(V[u])
        findv = find(V[v])
        if findu != findv:
            X.append(edge)
            sum += edge[0]
            union(V[u], V[v])
            clusters -= 1

    # find the last edge nto making a loop
    edge_makes_loop = True
    last_edge = 0
    while edge_makes_loop:
        edge = heapq.heappop(h)
        node_tuple = edge[1]
        u = node_tuple[0]
        v = node_tuple[1]

        findu = find(V[u])
        findv = find(V[v])
        if findu != findv:
            edge_makes_loop = False
            last_edge = edge[0]

    return last_edge


def create_graph(x, y):
    adj = [[] for i in range(len(x))]
    weight = [[] for i in range(len(x))]

    for i in range(len(x)):
        for j in range(len(x)):
            #there's a way to optimize the computation by mirroring the matrix ixj by its diagonal
            if i == j:
                continue

            adj[i].append(j)
            weight[i].append(distance(x, y, i, j))

    return adj, weight


def distance(x, y, node1, node2):
    return math.sqrt(math.pow(x[node1] - x[node2], 2) + math.pow(y[node1] - y[node2], 2))


def make_set(x):
    x.parent = x
    x.rank = 0


def find(x):
    if x.parent != x:
        x.parent = find(x.parent)

    return x.parent


def union(x, y):
    x_root = find(x)
    y_root = find(y)

    if x_root == y_root:
        return

    if x_root.rank < y_root.rank:
        x_root.parent = y_root
    elif x_root.rank > y_root.rank:
        y_root.parent = x_root
    else:
        y_root.parent = x_root
        y_root.rank = x_root.rank + 1



if __name__ == '__main__':
    input = sys.stdin.read()
    #input = "12\n7 6\n4 3\n5 1\n1 7\n2 7\n5 7\n3 3\n7 8\n2 8\n4 4\n6 7\n2 6\n3\n"
    #input = "8\n3 1\n1 2\n4 6\n9 8\n9 9\n8 9\n3 11\n4 12\n4\n"

    data = list(map(int, input.split()))
    n = data[0]
    data = data[1:]
    x = data[0:2 * n:2]
    y = data[1:2 * n:2]
    data = data[2 * n:]
    k = data[0]
    print("{0:.9f}".format(clustering(x, y, k)))
