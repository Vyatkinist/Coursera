#Uses python3
import sys
import math
import heapq

class Expando(object):
    pass


def minimum_distance_kruskal(x, y):
    # all vertices as disjoint sets
    V = [Expando() for i in range(len(x))]
    for vertex in V:
        make_set(vertex)

    # use heap to sort the weights and add unique edges
    h = []
    # todo the cycles can be cut in half to avoid going through i >= j cases
    for i in range(len(x)):
        for j in range(len(y)):
            # go through only part of the ixj matrix upper than its diagonal, where i < j
            if i < j:
                w = distance(x, y, i, j)
                # the data stored has the format (weight, (node1, node2))
                heapq.heappush(h, (w, (i, j)))

    X = []
    sum = 0
    while len(h) > 0:
        edge = heapq.heappop(h)
        node_tuple = edge[1]
        u = node_tuple[0]
        v = node_tuple[1]

        findu = find(V[u])
        findv = find(V[v])
        if findu != findv:
            X.append(edge)
            sum += edge[0]
            union(V[u], V[v])

    return sum


def create_graph(x, y):
    adj = [[] for i in range(len(x))]
    weight = [[] for i in range(len(x))]

    for i in range(len(x)):
        for j in range(len(x)):
            #there's a way to optimize the computation by mirroring the matrix ixj by its diagonal
            if i == j:
                continue

            adj[i].append(j)
            weight[i].append(distance(x, y, i, j))

    return adj, weight


def distance(x, y, node1, node2):
    return math.sqrt(math.pow(x[node1] - x[node2], 2) + math.pow(y[node1] - y[node2], 2))


def make_set(x):
    x.parent = x
    x.rank = 0


def find(x):
    if x.parent != x:
        x.parent = find(x.parent)

    return x.parent


def union(x, y):
    x_root = find(x)
    y_root = find(y)

    if x_root == y_root:
        return

    if x_root.rank < y_root.rank:
        x_root.parent = y_root
    elif x_root.rank > y_root.rank:
        y_root.parent = x_root
    else:
        y_root.parent = x_root
        y_root.rank = x_root.rank + 1



if __name__ == '__main__':
    input = sys.stdin.read()
    #input = "4\n0 0\n0 1\n1 0\n1 1"
    #input = "5\n0 0\n0 2\n1 1\n3 0\n3 2"

    data = list(map(int, input.split()))
    n = data[0]
    x = data[1::2]
    y = data[2::2]
    print("{0:.9f}".format(minimum_distance_kruskal(x, y)))
