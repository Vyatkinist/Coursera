function [ alpha ] = laff_dot( x, y )

% make vector x and y the same type (row or column)
[ m_y, n_y ] = size( y );
[ m_x, n_x ] = size( x );

if (m_y == 1 && m_x ~= 1) || (n_y == 1 && n_x ~= 1)
    x = x';
    [ m_x, n_x ] = size( x );
end

% check if inputs are valid
if (m_y ~= m_x) || (n_y ~= n_x)
    alpha = 'FAILED';
    return
end

alpha = 0;
if m_y == 1
    for i=1:n_y
        alpha = alpha + (x(1, i) * y(1, i));
    end
else
    for i=1:m_y
        alpha = alpha + (x(i, 1) * y(i, 1));
    end
end

end

