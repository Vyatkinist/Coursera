function [ alpha ] = laff_norm2( x, y )

dot = laff_dot(x, y);
alpha = sqrt(dot);

end

