function [ x_out ] = laff_scal( alpha, x )
if ~isscalar(alpha) || ~isvector(x)
    x_out = 'FAILED';
    return
end

x_out = x;
[ m_x, n_x ] = size( x );
if m_x == 1 % if it's a row vertor
    for i=1:n_x
        x_out(1, i) = x(1, i) * alpha;
    end
else % column vector
    for i=1:m_x
        x_out(i, 1) = x(i, 1) * alpha;
    end
end

